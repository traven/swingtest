import java.util.HashSet;
import javax.swing.JTextArea;

public final class Writer {

	private HashSet<JTextArea> textSet = new HashSet<JTextArea>();
	private static volatile Writer instance = null;

	private Writer() {

	}

	public static Writer getInstance() {
		if (instance == null)
			synchronized (Writer.class) {
				if (instance == null)
					instance = new Writer();
			}
		return instance;
	}

	public void register(JTextArea text) {
		textSet.add(text);
	}
	
	public void send(String str){
		for(JTextArea tArea: textSet ){
			tArea.append(str);
		}
	}
}
