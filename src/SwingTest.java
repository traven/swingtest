import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class SwingTest extends JFrame {
	
	ServerSocketThread server;
	Socket client;
	private int port = 1025;
	PrintWriter printWriter;
	JButton button;
	JTextArea text;
	final JTextField field = new JTextField();
	
	public SwingTest() {
		boolean serverUp = false;
		initUI();
		while (!serverUp) {
			try {
				server = new ServerSocketThread(port);
				server.start();
				serverUp = true;
			} catch (Exception e) {
				port++;
			}
			System.out.println(port);
		}
	}

	public final void initMenu() {
		JMenuBar menubar = new JMenuBar();
		ImageIcon icon = new ImageIcon(getClass()
				.getResource("Images/exit.png"));

		JMenu file = new JMenu("File");
		file.setMnemonic(KeyEvent.VK_F);

		JMenuItem eMenuItem = new JMenuItem("Exit", icon);
		eMenuItem.setMnemonic(KeyEvent.VK_C);
		eMenuItem.setToolTipText("Exit application");
		eMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}

		});

		JMenuItem dMenuItem = new JMenuItem("Don't exit");
		dMenuItem.setMnemonic(KeyEvent.VK_D);
		dMenuItem.setToolTipText("Don't exit application");
		dMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("not exiting");

			}
		});

		// adding to menu
		file.add(eMenuItem);
		file.add(dMenuItem);

		// adding menu to menubar
		menubar.add(file);
		setJMenuBar(menubar);

	}

	public final void initUI() {

		// creating menubar
		initMenu();

		JPanel mPanel = new JPanel();
		mPanel.setLayout(new BoxLayout(mPanel, BoxLayout.Y_AXIS));
		setContentPane(mPanel);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		text = new JTextArea();
		Writer.getInstance().register(text);
		
		field.setText("/connect localhost 1025");
		field.setMaximumSize(new Dimension(100, 16));
		
		
		ImageIcon bIcon = new ImageIcon(getClass()
				.getResource("Images/add.png"));
		button = new JButton("Click", bIcon);
		
		
		mPanel.add(text);
//		mPanel.add(Box.createHorizontalGlue());
//		mPanel.add(Box.createVerticalGlue());
//		mPanel.se
		field.setAlignmentX(CENTER_ALIGNMENT);
		button.setAlignmentX(CENTER_ALIGNMENT);
		text.setAlignmentX(CENTER_ALIGNMENT);
		//text.setMaximumSize(new Dimension(300,200));
		text.setRows(4);
		text.setColumns(10);
		mPanel.add(field);
		mPanel.add(button);
		setTitle("Simple menu");
		setSize(600, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		addListeners();
	}
	
	private void addListeners(){ 
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String readText = field.getText();
				if (readText.startsWith("/connect")) {
					String[] split = readText.split(" ");
					text.append("connecting to " + split[1] + "...\n");
					// client.connect(InetAddress.getByName(split[1]));
					try {
						client = new Socket(split[1], Integer
								.parseInt(split[2]));
						printWriter = new PrintWriter(client.getOutputStream());
						System.out.println("connected succesfully");
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					printWriter.println(readText);
					printWriter.flush();
					System.out.println(readText);
				}
			}
		});
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				SwingTest ex = new SwingTest();
				ex.setVisible(true);
			}
		});

	}
}