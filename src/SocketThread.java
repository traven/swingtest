import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketThread extends Thread {

	private BufferedReader stream;
	private Writer writer = Writer.getInstance();

	public SocketThread(Socket s) {
		try {
			stream = new BufferedReader(new InputStreamReader(
					s.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while (true) {
				writer.send(stream.readLine());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
