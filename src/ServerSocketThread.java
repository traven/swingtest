import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketThread extends Thread {

	private ServerSocket socket;
	private Boolean flag = true;
	

	public ServerSocketThread(int port) throws IOException {
			socket = new ServerSocket(port);
	}

	public void halt() {
			flag = false;
	}

	@Override
	public void run() {
		while (flag) {
			try {
				Socket s = socket.accept();
				new SocketThread(s).start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}

}
